package com.example.rany.intentdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.rany.intentdemo.model.Employee;

import java.util.ArrayList;

public class SecondScreen extends AppCompatActivity {

    public static final String TAG = "oooo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_screen);

        Employee e = getIntent().getParcelableExtra("Emp");
        Toast.makeText(this, e.getName(), Toast.LENGTH_LONG).show();

        ArrayList<Employee> employees =
                getIntent().getParcelableArrayListExtra("employee");

        for (Employee em : employees){
            Log.e("employee", em.toString());
        }



////        String name = getIntent().getStringExtra("name");
////        int psw = getIntent().getIntExtra("password", 0);
//
//        Toast.makeText(this, name+psw , Toast.LENGTH_LONG).show();

    }
}
