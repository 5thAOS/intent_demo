package com.example.rany.intentdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class LoginScreen extends AppCompatActivity {

    Button btnLogin;
    EditText edtName, edtPsw;
    String name, psw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        btnLogin = findViewById(R.id.btnLogin);
        edtName = findViewById(R.id.edtUsername);
        edtPsw = findViewById(R.id.edtPassword);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = edtName.getText().toString();
                psw = edtPsw.getText().toString();

                HashMap<String, String> user = new HashMap<>();
                user.put("Admin", "123456");
                user.put("Student", "student123");
                user.put("Manager", "gm9999");

                Intent intent = new Intent(LoginScreen.this, LogoutScreen.class);
                for (Map.Entry<String, String> users : user.entrySet()){
                    Toast.makeText(LoginScreen.this, name+ " "+psw, Toast.LENGTH_SHORT).show();

                    if(name.equalsIgnoreCase(users.getKey()) && psw.equalsIgnoreCase(users.getValue())){
                        intent.putExtra("User", name);
                        intent.putExtra("Psw", psw);
                        Log.e("oooo", "Intent");
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(LoginScreen.this, "Login Failed!", Toast.LENGTH_SHORT).show();
                    }
                    break;
                }

            }
        });

    }

}
