package com.example.rany.intentdemo;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class ImplicitIntentDemo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_implicit_intent_demo);

        Intent i = new Intent("Open Web browser", Uri.parse("http://www.khmeracademy.org"));

        //startActivity(i);
        //Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:098990282"));
        //Intent chooser = Intent.createChooser(i, "Please choose app below");

        if(i.resolveActivity(getPackageManager())!= null){
            startActivity(i);
        }
        else{
            Toast.makeText(this, "Invalid Action", Toast.LENGTH_SHORT).show();
        }
    }
}
