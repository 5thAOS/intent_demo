package com.example.rany.intentdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.rany.intentdemo.model.Employee;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

//    @BindView(R.id.btnGo)
//    Button btnGo;

    Button btnGo, button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        ButterKnife.bind(this);

        btnGo = findViewById(R.id.btnGo);
        button = findViewById(R.id.button);
        button.setOnClickListener(this);
        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData();
            }
        });
    }

    private void sendData(){

        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1, "Sok", "Teacher", 600));
        employees.add(new Employee(2, "Bopha", "Doctor", 2000));
        employees.add(new Employee(3, "Chhylin", "Developer", 1500));

        Intent i = new Intent(this, SecondScreen.class);
        i.putExtra("Emp", new Employee(1, "Kanha", "Singer", 2500));
        i.putParcelableArrayListExtra("employee",employees);
        startActivity(i);
    }


    @Override
    public void onClick(View v) {
        Toast.makeText(this, "Click ME", Toast.LENGTH_SHORT).show();
    }

    public void ClickMe(View view) {
        Toast.makeText(this, "Button", Toast.LENGTH_SHORT).show();
    }
}
